#pragma once


#include <Application.hpp>
#include <glApi.hpp>
#include <ObjLoader.hpp>
#include <utils.hpp>


#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stb_image.h>


#include <iostream>
#include <memory>
#include <cstdlib>
#include <ctime>


#include "GuiElement.hpp"
#include "RenderObject.hpp"
#include "model/Asteroid.hpp"
#include "model/LifeGui.hpp"
#include "model/Missile.hpp"
#include "model/Player.hpp"
#include "model/Spaceship.hpp"


#define WIN_NOT_WIN_YET -1 // In case no one won yet
#define WIN_PLAYER_1     1 // In case Player 1 won
#define WIN_PLAYER_2     2 // In case Player 2 won
#define WIN_DRAW         3 // In case of a draw

#define SKYBOX_SIZE 400    // Semi-scale of the skybox


class RenderObject; // Forward declaration of RenderObject class


/**
 * @brief Class used to handle the whole application
 * Glitter's-Application-derived class
 */
class GlApplication : public Application {
    private:
        /**
         * @brief Enum containing every model key
         * Enum containing every key used to retrieve models but also entities of the same type
         */
        enum ModelKeys {
            SPACESHIP_1, // Player one's spaceship
            SPACESHIP_2, // Player two's spaceship
            LASER_1,     // Laser shot by player one's spaceship 
            LASER_2,     // Laser shot by player two's spaceship
            ASTEROID,    // Asteroid
            SKYBOX,      // Skybox
            PLANKS,      // Glitter's plank
            TRON,        // Glitter's tron
            OBB          // Oriented Bounding Box
        };

        bool drawBoundingBoxes; 

        std::map<ModelKeys, std::vector<std::shared_ptr<Entity>>> m_entities;  // objects to render
        std::map<ModelKeys, std::unique_ptr<RenderObject>> m_models;           // loaded models
        std::vector<std::shared_ptr<GuiElement>> m_guiElement;                 // gui elements

        glm::mat4 m_proj;                                                      // Projection matrix
        glm::mat4 m_view;                                                      // worldView matrix

        float m_eyePhi;                                                        // Camera position longitude angle
        float m_eyeTheta;                                                      // Camera position latitude angle
        float m_currentTime;                                                   // elapsed time since first frame

        int m_framebufferWidth;                                                // Window's width
        int m_framebufferHeight;                                               // Window's height

        std::shared_ptr<Spaceship> m_spaceshipPlayer1;                         // Player 1 spaceship
        std::shared_ptr<Spaceship> m_spaceshipPlayer2;                         // Player 2 spaceship

        std::shared_ptr<GuiElement> win;                                       // Screen to display to the winner
        std::shared_ptr<GuiElement> lose;                                      // Screen to display to the loser
        std::shared_ptr<GuiElement> draw;                                      // Screen to display to both players in case of draw

        std::shared_ptr<LifeGui> healthbar;                                    // Health bar GUI element

        int winner;                                                            // Winning player

        Program guiProgram;                                                    // Glitter program used to render the GUI
    
    public:
        GlApplication(int windowWidth, int windowHeight);
        void setCallbacks() override;
        static void usage(std::string & shortDescritpion, std::string & synopsis, std::string & description);

        void setFramebufferSize(int framebufferWidth, int framebufferHeight);
        void quickLog() const;
        void toggleHitboxes();

    private:
        void loadModels();
        void createEntities();

        void renderFrame() override;
        void renderGameFrame();
        void renderGameEndedFrame();
        void renderGUI();

        void update() override;
        void updateGame();
        void updateGameEnded();
        static void resize(GLFWwindow * window, int framebufferWidth, int framebufferHeight);
        static void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods);
        void continuousKey(float deltaTime);
        void computeView(Player player);

        void processPlayer1Input(GLFWwindow *window, float deltaTime);
        void processPlayer2Input(GLFWwindow *window, float deltaTime);

        void resetGame();
};
