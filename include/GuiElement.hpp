#pragma once


#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include <stb_image.h>


#include "glApi.hpp"
#include "utils.hpp"


/**
 * @brief Base class for every GUI element
 * Class inherited by any GUI element destined to be rendered directly on-screen
 */
class GuiElement {
	private:
		glm::vec2 position;               // (x,y) Position on the screen
		glm::vec2 dimension;              // (x,y) Scale on the screen

		Sampler textureSampler;           // Texture sampler linked to the GUI element
		std::shared_ptr<Texture> texture; // Smart pointer to glitter's Texture of the Element
		VAO vao;                          // VAO of the GUI Element

		glm::mat4 modelMatrix;            // Basic model matrix to render the element

	public: 
		GuiElement(Program &program, const std::string &texturePath);
		GuiElement(Program &program);

		void setPosition(glm::vec2 position);
		void setDimension(glm::vec2 dimension);

		void draw();
		void updateUniforms(Program &guiProgram);

	protected:
		void setTexture(std::shared_ptr<Texture> &newTexture);
	
	private:
			void computeModelMatrix();
			void createElement(Program &program);
};
