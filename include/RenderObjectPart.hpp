#pragma once


#include <glApi.hpp>


#include <memory>


/**
 * @brief Class representing a sub-part of an object
 * Class representing a sub-RenderObject
 */
class RenderObjectPart {
    private:
        std::shared_ptr<VAO>     m_vao;             // Smart pointer to corresponding RenderObject's VAO
        std::shared_ptr<Program> m_program;         // Smart pointer to corresponding RenderObject's OpenGL Program
        std::shared_ptr<Texture> m_diffuseTexture;  // Smart pointer to corresponding RenderObject's Diffuse map
        std::shared_ptr<Texture> m_normalTexture;   // Smart pointer to corresponding RenderObject's normal map
        std::shared_ptr<Texture> m_specularTexture; // Smart pointer to corresponding RenderObject's specular map
    
    public:
        RenderObjectPart() = delete;
        RenderObjectPart(const RenderObjectPart &) = delete;
        RenderObjectPart(RenderObjectPart &&) = default;
        RenderObjectPart(std::shared_ptr<VAO> vao, std::shared_ptr<Program> program, std::shared_ptr<Texture> texture, std::shared_ptr<Texture> ntexture, std::shared_ptr<Texture> stexture);

        void draw(Sampler * colormap, Sampler * normalmap, Sampler * specularmap);
        void update(const glm::mat4 & proj, const glm::mat4 & view, const glm::mat4 & mw);

        glm::vec3 get_dimensions() const;
};
