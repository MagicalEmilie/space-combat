#pragma once


#include <glApi.hpp>
#include <ObjLoader.hpp>
#include <utils.hpp>


#include <stb_image.h>


#include <memory>


#include "RenderObjectPart.hpp"


/**
 * @brief The RenderObject class
 *
 * A RenderObject is split into parts, sharing the same geometry (VBOs), but referencing different primitive subsets (IBO) and materials (textures, ...).
 */
class RenderObject {
	private:
		std::vector<RenderObjectPart> m_parts;  // Sub-parts of the object
		
		std::unique_ptr<Sampler> m_diffusemap;  // Diffuse map of the object
		std::unique_ptr<Sampler> m_normalmap;   // Normal map of the object
		std::unique_ptr<Sampler> m_specularmap; // Specular map of the object

		std::shared_ptr<Program> program;       // OpenGL program to render the object

		glm::vec3 dimensions;                   // Dimensions of the object (used to compute OBBs)

	public:
		RenderObject(const RenderObject &) = delete;

		static std::unique_ptr<RenderObject> createCheckerBoardPlaneInstance(const glm::mat4 & modelWorld);
		/**
		 * @brief creates an instance from a wavefront file and modelWorld matrix
		 * @param objname the filename of the wavefront file
		 * @param modelWorld the matrix transform between the object (a.k.a model) space and the world space
		 * @return the created RenderObject as a smart pointer
		 */
		static std::unique_ptr<RenderObject> createWavefrontInstance(const std::string & objname);

		/**
		 * @brief Sets all uniform variables related to material and lighting
		 * @param program
		 * @param material
		 *
		 * @note Besides the material, three directional lights (defined in world space) are passed to the GLSL program.
		 */
		void setProgramMaterial(std::shared_ptr<Program> & program, const SimpleMaterial & material) const;

		/**
		 * @brief Draw this RenderObject
		 */
		void draw();

		/**
		 * @brief update the program MVP uniform variable
		 * @param proj the projection matrix
		 * @param view the worldView matrix
		 */
		void update(const glm::mat4 & proj, const glm::mat4 & view, const glm::mat4 &model);

		void setDisableLightning(const bool &disableLightning);

		glm::vec3 get_dimensions() const;

	private:
		RenderObject();
		void loadWavefront(const std::string & objname);
};
