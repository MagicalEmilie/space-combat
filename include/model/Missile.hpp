#pragma once


#include "glm/glm.hpp"


#include "model/Entity.hpp"


#define MISSILE_SPEED    30.0 // Speed of the missile 
#define MISSILE_COOLDOWN  0.3 // Cooldown between two shots


/**
 * @brief Class representing a missile that a spaceship shot
 * Entity-derived missile class
 */
class Missile : public Entity {
    public:
        Missile(const Entity* const shotBy);
        ~Missile();

        void update(const float &deltaTime) override;
};
