#pragma once


#include <cstdlib>
#include <ctime>


#include <glm/ext.hpp>


#include "model/Entity.hpp"


#define ASTEROID_SPEED 0.1 // Speed of asteroid rotation


/**
 * @brief Class representing an Asteroid
 * Entity-derived asteroid class
 */
class Asteroid : public Entity {
    private:
        glm::vec3 rotationAngle; // Euler angles used to rotate the asteroid

    public:
        Asteroid();
        ~Asteroid();

        void update(const float &deltaTime) override;
};
