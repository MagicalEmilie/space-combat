#pragma once


#include <glApi.hpp>


#include "GuiElement.hpp"


/**
 * @brief GUI Element representing a ship's remaining health points
 * GuiElement-derived
 */
class LifeGui : public GuiElement {
	private:
		std::shared_ptr<Texture> hp_3_texture; // Texture representing 3 health points
		std::shared_ptr<Texture> hp_2_texture; // Texture representing 2 health points
		std::shared_ptr<Texture> hp_1_texture; // Texture representing 1 health points

	public:
		LifeGui(Program &program);

		void setCurrentLife(const unsigned int &life);
};
