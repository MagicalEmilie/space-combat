#!/bin/bash

pandoc rapport.md -o Rapport.pdf --toc --template eisvogel.tex --listings --number-sections --top-level-division=chapter
