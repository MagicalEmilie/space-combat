#include "model/Spaceship.hpp"


/**
 * @brief Constructs a new Spaceship object
 * Constructor to a Spaceship object taking a player (the pilot) and a model matrix
 * @param playerNumber 
 * @param modelWorld 
 */
Spaceship::Spaceship(const Player &playerNumber, const glm::mat4 &modelWorld)
        : Entity(),
          thrusting(false),
          blocked(false),
          acceleration(1), 
          playerNumber(playerNumber),
          fireCooldown(0),
          healthPoints(3) {}

/**
 * @brief Method returning the ship's acceleration
 * Method returning the ship's acceleration
 * @return const float& The ship's acceleration
 */
const float &Spaceship::get_acceleration() const { return this->acceleration; }

/**
 * @brief Method returning the ship's owner
 * Method returning the ship's owner
 * @return const float& The ship's owner
 */
const Player &Spaceship::get_player_number() const { return this->playerNumber; }

/**
 * @brief Method wether returning the ship is currently thrusting
 * Method wether returning the ship is currently thrusting
 * @return true If the ship is thrusting
 * @return false If the ship is not thrusting
 */
const bool &Spaceship::is_thrusting() const { return this->thrusting; }

/**
 * @brief Method returning the ship's remaining health points
 * Method returning the ship's remaining health points
 * @return const unsigned int& The ship's remaining health points
 */
const unsigned int &Spaceship::get_health_points() const { return this->healthPoints; }

/**
 * @brief Method allowing to set the ship's current acceleration
 * Method allowing to set the ship's current acceleration
 * @param newAcceleration The new acceleration amount to give to the ship
 */
void Spaceship::set_acceleration(const float &newAcceleration) {
    this->acceleration = newAcceleration;
}

/**
 * @brief Method allowing to set wether the ship is currently thrusting
 * Method allowing to set wther the ship is currently thrusting
 * @param newAcceleration Wether the ship is currently thrusting
 */
void Spaceship::set_thrusting(const bool &newThrusting) {
    this->thrusting = newThrusting;
}

/**
 * @brief Method making the ship take damage
 * Maethod making the ship take a single damage by removing one of his remaining health points
 */
void Spaceship::take_damage() {
    this->healthPoints--;
}

/**
 * @brief Method handling the ship's thrusting behavious
 * Method handling the ship's thrusting behavious, checking if it is supposed to be able to thrust or not (by detecting future or currrent collisions)
 * @param deltaTime [Unused] Time elapsed since last loop
 * @param renderObjectThis The ship's corresponding renderObject
 * @param otherShip The other ship's corresponding Entity
 * @param renderObjectOtherShip The other ship's corresponding RenderObject
 */
void Spaceship::thrust(const float &deltaTime,
                       const std::unique_ptr<RenderObject> &renderObjectThis,
                       const Entity &otherShip,
                       const std::unique_ptr<RenderObject> &renderObjectOtherShip) {
    this->thrusting = true;
    if (!Entity::check_box_intersection(renderObjectThis, otherShip, renderObjectOtherShip, true, this->get_velocity(deltaTime), otherShip.get_velocity(deltaTime))) {
        this->blocked = false;
    } else {
        this->blocked = true;
    }
}

/**
 * @brief method updating the ship
 * Method overriding Entity::update updating the ship's thrusting state, acceleration amount, fire cooldown and position
 * @param deltaTime Time elapsed since last loop
 */
void Spaceship::update(const float &deltaTime) {
    this->fireCooldown -= deltaTime;
    
    if (thrusting) {
        if (this->acceleration < 1) {
            this->acceleration = 1;
        } else if (this->acceleration >= 10) {
            this->acceleration = 10;
        } else {
            this->acceleration *= 1.01;
        }
    } else {
        if (this->acceleration < 0) {
            this->acceleration = 0;
        } else {
            this->acceleration -= 0.05;
        }
    }

    this->thrusting = false;

    if (this->blocked) {
        this->acceleration = 0;
    } else {
        Entity::move(normalize(this->m_localZ)*this->acceleration*deltaTime);
    }
}

/**
 * @brief Method shooting a missile
 * Method shooting a missile by generating a new missile object and attaching the pilot to it, then setting up the cooldown
 * @return std::shared_ptr<Missile> 
 */
std::shared_ptr<Missile> Spaceship::generate_missile() {
    if(this->fireCooldown > 0) {
        return nullptr;
    }

    this->fireCooldown = MISSILE_COOLDOWN;

    return std::shared_ptr<Missile>(new Missile(this));
}

/**
 * @brief Method returning the ship velocity
 * Method returning the ship's velocity in all three axis, given a delta time amount (used in collision detection)
 * @param deltaTime Time elapsed since last loop
 * @return glm::vec3 
 */
glm::vec3 Spaceship::get_velocity(const float &deltaTime) const {
    return this->acceleration*glm::normalize(this->m_localZ)*deltaTime;
}
