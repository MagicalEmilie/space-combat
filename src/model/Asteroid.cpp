#include "model/Asteroid.hpp"


/**
 * @brief Constructs a new Astreroid object
 * Asteroid default constructor, using glm::pi and assigning random rotation Euler angles
 */
Asteroid::Asteroid()
        : Entity() {
    const float pi = glm::pi<float>();
    this->rotationAngle = glm::vec3(glm::radians((float)(rand() % 180)) - pi, glm::radians((float)(rand() % 180)) - pi, glm::radians((float)(rand() % 180)) - pi);
}

/**
 * @brief Détruit l'objet Asteroid
 * 
 */
Asteroid::~Asteroid() {}

/**
 * @brief Method updating the asteroid
 * method overriding Entity::update rotating the asteroid given a delta-time value
 * @param deltaTime 
 */
void Asteroid::update(const float &deltaTime) {
    glm::vec3 xAxis = (float)sin(this->rotationAngle.x * deltaTime * ASTEROID_SPEED) * this->get_x_axis();
    rotate(glm::quat(cos(this->rotationAngle.x * deltaTime * ASTEROID_SPEED), xAxis));
    
    glm::vec3 yAxis = (float)sin(this->rotationAngle.y * deltaTime * ASTEROID_SPEED) * this->get_y_axis();
    rotate(glm::quat(cos(this->rotationAngle.y * deltaTime * ASTEROID_SPEED), yAxis));
    
    glm::vec3 zAxis = (float)sin(this->rotationAngle.z * deltaTime * ASTEROID_SPEED) * this->get_z_axis();
    rotate(glm::quat(cos(this->rotationAngle.z * deltaTime * ASTEROID_SPEED), zAxis));
}
