#include "model/Missile.hpp"


/**
 * @brief Constructs a new Missile object
 * Constructor to a Missile object taking the player's ship that shot it in account
 * @param shotBy The spaceship that shot the missile
 */
Missile::Missile(const Entity* const shotBy)
        : Entity() { 
    m_position = shotBy->get_position();
    m_rotation = shotBy->get_rotation();
    m_localX = shotBy->get_x_axis();
    m_localY = shotBy->get_y_axis();
    m_localZ = shotBy->get_z_axis();

    computeModelMatrix();
}

/**
 * @brief Destroys the Missile object
 * Destroys the Missile object
 */
Missile::~Missile() {}

/**
 * @brief Method updating the missile
 * Method overriding Entity::update updating the missile's position. Currently, other behaviours are handled in the Application
 * class because of a lack of developpement time
 * @param deltaTime 
 */
void Missile::update(const float &deltaTime) {
    move(normalize(this->m_localZ) * 30.0f * deltaTime);
}
