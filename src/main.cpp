#define GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL

#include <iostream>
#include "GlApplication.hpp"

int main(int argc, char * argv[])
{
    GlApplication app(720, 480);
  
    app.setCallbacks();
    app.mainLoop();
}
