#include "GlApplication.hpp"

/**
 * @brief Constructs a new GlApplication object
 * Constructor to a GlApplication object, given it's window dimensions
 * @param windowWidth The application's window's width
 * @param windowHeight The application's window's height
 */
GlApplication::GlApplication(int windowWidth, int windowHeight) 
        : Application(windowWidth, windowHeight), 
          drawBoundingBoxes(false),
          guiProgram("shaders/gui.v.glsl", "shaders/gui.f.glsl"),
          m_currentTime(0),
          m_framebufferWidth(windowWidth), 
          m_framebufferHeight(windowHeight),
          m_spaceshipPlayer1(new Spaceship(Player::one, glm::mat4(1))),
          m_spaceshipPlayer2(new Spaceship(Player::two, glm::translate(glm::mat4(1), {0, 2, 2}))),
          winner(WIN_NOT_WIN_YET)
{
    srand(time(NULL));
    GLFWwindow * window = glfwGetCurrentContext();
    glfwGetFramebufferSize(window, &windowWidth, &windowHeight);
    resize(window, windowWidth, windowHeight);

    glEnable(GL_DEPTH_TEST);

    loadModels();
    createEntities();

    this->win = std::shared_ptr<GuiElement>(new GuiElement(guiProgram, "meshes/win.png"));
    this->lose = std::shared_ptr<GuiElement>(new GuiElement(guiProgram, "meshes/lose.png"));
    this->draw = std::shared_ptr<GuiElement>(new GuiElement(guiProgram, "meshes/draw.png"));

    this->healthbar = std::shared_ptr<LifeGui>(new LifeGui(guiProgram));
    this->healthbar->setPosition({0.005, 0.01});
    this->m_guiElement.push_back(healthbar);
}

/**
 * @brief Method reseting the game's state
 * Method resetting the game's state, respawning entities and erasing the winner
 */
void GlApplication::resetGame() {
    this->winner = WIN_NOT_WIN_YET;

    this->m_entities.clear();

    this->createEntities();
}

/**
 * @brief Method printing a quick log
 * Method `cout`-ing a quick log
 */
void GlApplication::quickLog() const {
    std:: cout << "LOG: SpaceshipPlayer2 {x=[" << this->m_spaceshipPlayer2->get_x_axis().x << ", " << this->m_spaceshipPlayer2->get_x_axis().y << ", " << this->m_spaceshipPlayer2->get_x_axis().z << "]; y=[" <<
    this->m_spaceshipPlayer2->get_y_axis().x << ", " << this->m_spaceshipPlayer2->get_y_axis().y << ", " << this->m_spaceshipPlayer2->get_y_axis().z << "]; z=[" <<
    this->m_spaceshipPlayer2->get_z_axis().x << ", " << this->m_spaceshipPlayer2->get_z_axis().y << ", " << this->m_spaceshipPlayer2->get_z_axis().z << "]}" << std::endl;
}

/**
 * @brief Method toggling OBB
 * A method toggling OBB in-world displaying
 */
void GlApplication::toggleHitboxes() {
    this->drawBoundingBoxes = !this->drawBoundingBoxes;
}

/**
 * @brief Method loading every models
 * Method loading every 3D model into the application's buffer
 */
void GlApplication::loadModels() {
    m_models[ModelKeys::SPACESHIP_1] = RenderObject::createWavefrontInstance("meshes/fighter/fighter_blue.obj");
    m_models[ModelKeys::SPACESHIP_2] = RenderObject::createWavefrontInstance("meshes/fighter/fighter_red.obj");
    
    m_models[ModelKeys::LASER_1] = RenderObject::createWavefrontInstance("meshes/laser_blue.obj");
    m_models[ModelKeys::LASER_1]->setDisableLightning(true);
    m_models[ModelKeys::LASER_2] = RenderObject::createWavefrontInstance("meshes/laser_red.obj");
    m_models[ModelKeys::LASER_2]->setDisableLightning(true);
   
    m_models[ModelKeys::SKYBOX] = RenderObject::createWavefrontInstance("meshes/skybox.obj");
    m_models[ModelKeys::SKYBOX]->setDisableLightning(true);

    m_models[ModelKeys::ASTEROID] = RenderObject::createWavefrontInstance("meshes/asteroid/asteroid.obj");
   
    //m_models[ModelKeys::PLANKS] = RenderObject::createWavefrontInstance("meshes/Pallet/Bswap_HPBake_Planks.obj");
   
    //m_models[ModelKeys::TRON] = RenderObject::createWavefrontInstance("meshes/Tron/TronLightCycle.obj");
    
    m_models[ModelKeys::OBB] = RenderObject::createWavefrontInstance("meshes/unit_box_v2.obj");
    m_models[ModelKeys::OBB]->setDisableLightning(true);
}

/**
 * @brief Method creating game's entities
 * Method spawning every entities, putting them into the application's buffer.
 */
void GlApplication::createEntities() {
    const float pi = glm::pi<float>();

    m_spaceshipPlayer1 = std::shared_ptr<Spaceship>(new Spaceship(Player::one, glm::mat4(1)));
    m_spaceshipPlayer2 = std::shared_ptr<Spaceship>(new Spaceship(Player::two, glm::translate(glm::mat4(1), {0, 2, 2})));

    m_spaceshipPlayer1->setScale(0.2f);
    m_spaceshipPlayer2->setScale(0.2f);
    m_spaceshipPlayer1->move({0, 0, -5});
    m_spaceshipPlayer2->move({0, 0,  5});

    glm::vec3 yAxis = (float)sin(pi/2) * this->m_spaceshipPlayer2->get_y_axis();
    this->m_spaceshipPlayer2->rotate(glm::quat(cos(pi/2), yAxis));

    m_entities[ModelKeys::SPACESHIP_1].push_back(m_spaceshipPlayer1);
    m_entities[ModelKeys::SPACESHIP_2].push_back(m_spaceshipPlayer2);

    for(int i = 0 ; i < 600 ; i++) {
        glm::vec3 position = {rand() % (SKYBOX_SIZE - 20) - ((SKYBOX_SIZE - 20)/2), rand() % (SKYBOX_SIZE - 20) - ((SKYBOX_SIZE - 20)/2), rand() % (SKYBOX_SIZE - 20) - ((SKYBOX_SIZE - 20)/2)};
        if(position.x >= -10 && position.x <= 10 && position.y >= -10 && position.y <= 10 && position.z >= -10 && position.z <= 10) {
            i--;
            continue;
        }

        std::shared_ptr<Entity> asteroidEntity(new Asteroid());
        asteroidEntity->setScale(0.25);
        asteroidEntity->move(position);
        m_entities[ModelKeys::ASTEROID].push_back(asteroidEntity);
    }

    /*std::shared_ptr<Entity> plankEntity(new Entity());
    plankEntity->move({2, 1, -0.1});
    plankEntity->rotate(glm::angleAxis(glm::radians(45.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
    m_entities[ModelKeys::PLANKS].push_back(plankEntity);*/

    /*std::shared_ptr<Entity> tronEntity(new Entity());
    tronEntity->move({1., 0, 0});
    tronEntity->rotate(glm::angleAxis(glm::radians(60.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
    tronEntity->setScale(0.25);
    m_entities[ModelKeys::TRON].push_back(tronEntity);*/
    
    std::shared_ptr<Entity> skybox(new Entity());
    skybox->setScale(SKYBOX_SIZE / 2);
    m_entities[ModelKeys::SKYBOX].push_back(skybox);
}

/**
 * @brief Method setting window callbacks
 * Method setting's every required GLFW callbacks
 */
void GlApplication::setCallbacks()
{
    GLFWwindow * window = glfwGetCurrentContext();
    glfwSetFramebufferSizeCallback(window, GlApplication::resize);
    glfwSetKeyCallback(window, GlApplication::keyCallback);
}

/**
 * @brief Method printing the usage of the game [DEPRECATED]
 */
void GlApplication::usage(std::string & shortDescription, std::string & synopsis, std::string & description)
{
    shortDescription = "Application for programming assignment 4";
    synopsis = "pa4";
    description = "  An application for texture mapping.\n"
        "  The following key bindings are available to interact with thi application:\n"
        "     <up> / <down>    increase / decrease latitude angle of the camera position\n"
        "     <left> / <right> increase / decrease longitude angle of the camera position\n"
        "     R                reset the view\n";
}

/**
 * @brief Method rendering correct frame
 * Metrhod choosing which kind of screen to render
 */
void GlApplication::renderFrame()
{
    if(winner == WIN_NOT_WIN_YET) {
        renderGameFrame();
    } else {
        renderGameEndedFrame();
    }    
}

/**
 * @brief Method rendering game frame
 * Method rendering the game scene as a split screen game
 */
void GlApplication::renderGameFrame() {
    glClearColor(1, 0, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, m_framebufferHeight / 2, m_framebufferWidth, m_framebufferHeight / 2+1);
    computeView(Player::one);
    for(std::pair<const GlApplication::ModelKeys, std::vector<std::shared_ptr<Entity>>> &keyValue : m_entities) {
        std::unique_ptr<RenderObject> &modelToRender = m_models[keyValue.first];
        std::unique_ptr<RenderObject> &boxModel      = m_models[ModelKeys::OBB];

        for(std::shared_ptr<Entity> entity : keyValue.second) {
            modelToRender->update(this->m_proj, this->m_view, entity->getModelMatrix());
            modelToRender->draw();

            if (this->drawBoundingBoxes) {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                boxModel->update(this->m_proj, this->m_view, entity->get_box_model_matrix(modelToRender));
                boxModel->draw();
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
        }
    }    
    this->healthbar->setCurrentLife(this->m_spaceshipPlayer1->get_health_points());
    renderGUI();

    glViewport(0, 0, m_framebufferWidth, m_framebufferHeight / 2);
    computeView(Player::two);
    for(std::pair<const GlApplication::ModelKeys, std::vector<std::shared_ptr<Entity>>> &keyValue : m_entities) {
        std::unique_ptr<RenderObject> &modelToRender = m_models[keyValue.first];
        std::unique_ptr<RenderObject> &boxModel      = m_models[ModelKeys::OBB];
        
        for(std::shared_ptr<Entity> &entity : keyValue.second) {
            modelToRender->update(this->m_proj, this->m_view, entity->getModelMatrix());
            modelToRender->draw();

            if (this->drawBoundingBoxes) {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                boxModel->update(this->m_proj, this->m_view, entity->get_box_model_matrix(modelToRender));
                boxModel->draw();
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
        }
    }
    this->healthbar->setCurrentLife(this->m_spaceshipPlayer2->get_health_points());
    renderGUI();
}

/**
 * @brief Method rendering end-game screens
 * Method choosing which end-game screen to render then properly rendering it
 */
void GlApplication::renderGameEndedFrame() {
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Player 1
    glViewport(0, m_framebufferHeight / 2, m_framebufferWidth, m_framebufferHeight / 2+1);
    guiProgram.bind();
    if(winner == WIN_PLAYER_1) {
        this->win->updateUniforms(guiProgram);
        this->win->draw();
    } else if(winner == WIN_PLAYER_2) {
        this->lose->updateUniforms(guiProgram);
        this->lose->draw();
    } else {
        this->draw->updateUniforms(guiProgram);
        this->draw->draw();
    }
    guiProgram.unbind();

    // Player 2
    glViewport(0, 0, m_framebufferWidth, m_framebufferHeight / 2);
    guiProgram.bind();
    if(winner == WIN_PLAYER_1) {
        this->lose->updateUniforms(guiProgram);
        this->lose->draw();
    } else if(winner == WIN_PLAYER_2) {
        this->win->updateUniforms(guiProgram);
        this->win->draw();
    } else {
        this->draw->updateUniforms(guiProgram);
        this->draw->draw();
    }
    guiProgram.unbind();
}

/**
 * @brief Method rendering a GUI
 * Method getting every element to render then rendering them
 */
void GlApplication::renderGUI() {
    glDisable(GL_DEPTH_TEST);

    guiProgram.bind();
    for(std::shared_ptr<GuiElement> &element : this->m_guiElement) {
        element->updateUniforms(guiProgram);
        element->draw();
    }
    guiProgram.unbind();

    glEnable(GL_DEPTH_TEST);
}

/**
 * @brief Method updating the window
 * Method updating the window
 */
void GlApplication::update()
{
    if(winner == WIN_NOT_WIN_YET) {
        updateGame();
    } else {
        updateGameEnded();
    }
}

/**
 * @brief Method updating the game
 * Method updating the game by updating every entities and handling somne behaviours
 */
void GlApplication::updateGame() {
    float prevTime = m_currentTime;
    m_currentTime = glfwGetTime();
    float deltaTime = m_currentTime - prevTime;

    continuousKey(deltaTime);

    for(std::pair<const GlApplication::ModelKeys, std::vector<std::shared_ptr<Entity>>> &keyValue : m_entities) {
        for(std::shared_ptr<Entity> &entity : keyValue.second) {
            entity->update(deltaTime);
        }
    }

    bool didP1Die = false;
    bool didP2Die = false;
 
    for (int index = this->m_entities[ModelKeys::LASER_1].size()-1; index != -1; --index) {
        std::shared_ptr<Entity> missile = this->m_entities[ModelKeys::LASER_1].at(index);
        if (missile->check_box_intersection(this->m_models[ModelKeys::LASER_1], *this->m_spaceshipPlayer2.get(), this->m_models[ModelKeys::SPACESHIP_2])) {
            std::cout << "Player 1 hit player 2 !" << std::endl;
            this->m_entities[ModelKeys::LASER_1].erase(this->m_entities[ModelKeys::LASER_1].begin()+index);
            this->m_spaceshipPlayer2->take_damage();
            std::cout << "HP restants : " << this->m_spaceshipPlayer2->get_health_points() << std::endl;
            
            if (this->m_spaceshipPlayer2->get_health_points() == 0) {
                didP2Die = true;
            }
        }

        for (int asteroidIndex = 0; asteroidIndex < this->m_entities[ModelKeys::ASTEROID].size(); ++asteroidIndex) {
            std::shared_ptr<Entity> asteroid = this->m_entities[ModelKeys::ASTEROID][asteroidIndex];
            if (missile->check_box_intersection(this->m_models[ModelKeys::LASER_1], *asteroid.get(), this->m_models[ModelKeys::ASTEROID])) {
                this->m_entities[ModelKeys::LASER_1].erase(this->m_entities[ModelKeys::LASER_1].begin()+index);
                std::cout << "A missile hit an asteroid !" << std::endl;
            }
        }

        if (!missile->check_box_intersection(this->m_models[ModelKeys::LASER_1], *this->m_entities[ModelKeys::SKYBOX][0], this->m_models[ModelKeys::SKYBOX])) {
                this->m_entities[ModelKeys::LASER_1].erase(this->m_entities[ModelKeys::LASER_1].begin()+index);
                std::cout << "A missile went beyond the limits" << std::endl;
        }
    }

    for (int index = this->m_entities[ModelKeys::LASER_2].size()-1; index != -1; --index) {
        std::shared_ptr<Entity> missile = this->m_entities[ModelKeys::LASER_2].at(index);
        if (missile->check_box_intersection(this->m_models[ModelKeys::LASER_2], *this->m_spaceshipPlayer1.get(), this->m_models[ModelKeys::SPACESHIP_1])) {
            std::cout << "Player 2 hit player 1 !" << std::endl;
            this->m_entities[ModelKeys::LASER_2].erase(this->m_entities[ModelKeys::LASER_2].begin()+index);
            this->m_spaceshipPlayer1->take_damage();
            std::cout << "HP restants : " << this->m_spaceshipPlayer1->get_health_points() << std::endl;
            
            if (this->m_spaceshipPlayer1->get_health_points() == 0) {
                didP1Die = true;
            }
        }

        for (int asteroidIndex = 0; asteroidIndex < this->m_entities[ModelKeys::ASTEROID].size(); ++asteroidIndex) {
            std::shared_ptr<Entity> asteroid = this->m_entities[ModelKeys::ASTEROID][asteroidIndex];
            if (missile->check_box_intersection(this->m_models[ModelKeys::LASER_2], *asteroid.get(), this->m_models[ModelKeys::ASTEROID])) {
                this->m_entities[ModelKeys::LASER_2].erase(this->m_entities[ModelKeys::LASER_2].begin()+index);
                std::cout << "A missile hit an asteroid !" << std::endl;
            }
        }

        if (!missile->check_box_intersection(this->m_models[ModelKeys::LASER_2], *this->m_entities[ModelKeys::SKYBOX][0], this->m_models[ModelKeys::SKYBOX])) {
                this->m_entities[ModelKeys::LASER_2].erase(this->m_entities[ModelKeys::LASER_2].begin()+index);
                std::cout << "A missile went beyond the limits" << std::endl;
        }
    }
    
    GLFWwindow *window = glfwGetCurrentContext();
    if (didP1Die && didP2Die) {
        std::cout << "Égalité parfaite ! Incroyable !" << std::endl;
        winner = WIN_DRAW;
        std::cout << "Spaceship 1 hp : " << m_spaceshipPlayer1->get_health_points() << "\nSpaceship 2 hp : " << m_spaceshipPlayer2->get_health_points() << std::endl;
    } else if (didP1Die) {
        std::cout << "Victoire du joueur 2 !" << std::endl;
        winner = WIN_PLAYER_2;
        std::cout << "Spaceship 1 hp : " << m_spaceshipPlayer1->get_health_points() << "\nSpaceship 2 hp : " << m_spaceshipPlayer2->get_health_points() << std::endl;
    } else if (didP2Die) {
        std::cout << "Victoire du joueur 1 !" << std::endl;
        winner = WIN_PLAYER_1;
        std::cout << "Spaceship 1 hp : " << m_spaceshipPlayer1->get_health_points() << "\nSpaceship 2 hp : " << m_spaceshipPlayer2->get_health_points() << std::endl;
    }
}

/**
 * @brief Method updateing end-game screens
 */
void GlApplication::updateGameEnded() {
    GLFWwindow *window = glfwGetCurrentContext();

    if (glfwGetKey(window, GLFW_KEY_ENTER) == GLFW_PRESS) {
        resetGame();
    }
}

/**
 * @brief Method computing the view matrixes
 * Method implementing cameras and computing the screen's view matrices
 * @param player the player to compute the view matrix's
 */
void GlApplication::computeView(Player player)
{
    if (player == Player::one) {
        glm::vec3 eyePos = this->m_spaceshipPlayer1->get_position() - normalize(this->m_spaceshipPlayer1->get_z_axis())*2.0f - this->m_spaceshipPlayer1->get_y_axis();
        glm::vec3 up     = this->m_spaceshipPlayer1->get_y_axis();
        glm::vec3 center = this->m_spaceshipPlayer1->get_position();
        m_view = glm::lookAt(eyePos, center, up);
    } else if (player == Player::two) {
        glm::vec3 eyePos = this->m_spaceshipPlayer2->get_position() - normalize(this->m_spaceshipPlayer2->get_z_axis())*2.0f - this->m_spaceshipPlayer2->get_y_axis();
        glm::vec3 up     = this->m_spaceshipPlayer2->get_y_axis();
        glm::vec3 center = this->m_spaceshipPlayer2->get_position();
        m_view = glm::lookAt(eyePos, center, up);
    }
}

void GlApplication::continuousKey(float deltaTime)
{
    GLFWwindow *window = glfwGetCurrentContext();

    const float pi = glm::pi<float>();
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        m_eyeTheta += deltaTime * pi;
        if (m_eyeTheta > pi - pi / 10) {
            m_eyeTheta = pi - pi / 10.;
        }
    } else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        m_eyeTheta -= deltaTime * pi;
        if (m_eyeTheta < pi / 10) {
            m_eyeTheta = pi / 10.;
        }
    }

    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        m_eyePhi += deltaTime * pi;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        m_eyePhi -= deltaTime * pi;
    }

    this->processPlayer1Input(window, deltaTime);
    this->processPlayer2Input(window, deltaTime);
}

void GlApplication::processPlayer1Input(GLFWwindow *window, float deltaTime) {
    float rads = glm::radians(40.0f*deltaTime);
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) { // Spaceship 1 thrusters
        this->m_spaceshipPlayer1->thrust(deltaTime, this->m_models[ModelKeys::SPACESHIP_1], *this->m_entities[ModelKeys::SPACESHIP_2][0], this->m_models[ModelKeys::SPACESHIP_2]);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) { // Spaceship 1 pitches down
        glm::vec3 xAxis = (float)sin(-rads/2) * this->m_spaceshipPlayer1->get_x_axis();
        this->m_spaceshipPlayer1->rotate(glm::quat(cos(-rads/2), xAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) { // Spaceship 1 pitches up
        glm::vec3 xAxis = (float)sin( rads/2) * this->m_spaceshipPlayer1->get_x_axis();
        this->m_spaceshipPlayer1->rotate(glm::quat(cos( rads/2), xAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) { // Spaceship 1 yaws left
        glm::vec3 yAxis = (float)sin(-rads/2) * this->m_spaceshipPlayer1->get_y_axis();
        this->m_spaceshipPlayer1->rotate(glm::quat(cos(-rads/2), yAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) { // Spaceship 1 yaws right
        glm::vec3 yAxis = (float)sin( rads/2) * this->m_spaceshipPlayer1->get_y_axis();
        this->m_spaceshipPlayer1->rotate(glm::quat(cos( rads/2), yAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) { // Spaceship 1 yaws left
        glm::vec3 zAxis = (float)sin(-rads/2) * this->m_spaceshipPlayer1->get_z_axis();
        this->m_spaceshipPlayer1->rotate(glm::quat(cos(-rads/2), zAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) { // Spaceship 1 yaws right
        glm::vec3 zAxis = (float)sin( rads/2) * this->m_spaceshipPlayer1->get_z_axis();
        this->m_spaceshipPlayer1->rotate(glm::quat(cos( rads/2),zAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) { // Spaceship 1 shoot
        std::shared_ptr<Missile> missile = m_spaceshipPlayer1->generate_missile();
        if(missile != nullptr) {
            m_entities[ModelKeys::LASER_1].push_back(missile);
        }
    } 
}

void GlApplication::processPlayer2Input(GLFWwindow *window, float deltaTime) {
    float rads = glm::radians(20.0f*deltaTime);
    if (glfwGetKey(window, GLFW_KEY_KP_ENTER) == GLFW_PRESS) { // Spaceship 2 thrusters
        this->m_spaceshipPlayer2->thrust(deltaTime, this->m_models[ModelKeys::SPACESHIP_2], *this->m_entities[ModelKeys::SPACESHIP_1][0], this->m_models[ModelKeys::SPACESHIP_1]);
    }
    if (glfwGetKey(window, GLFW_KEY_KP_5) == GLFW_PRESS) { // Spaceship 2 pitches down
        glm::vec3 xAxis = (float)sin(-rads/2) * this->m_spaceshipPlayer2->get_x_axis();
        this->m_spaceshipPlayer2->rotate(glm::quat(cos(-rads/2), xAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_KP_8) == GLFW_PRESS) { // Spaceship 2 pitches up
        glm::vec3 xAxis = (float)sin( rads/2) * this->m_spaceshipPlayer2->get_x_axis();
        this->m_spaceshipPlayer2->rotate(glm::quat(cos( rads/2), xAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_KP_4) == GLFW_PRESS) { // Spaceship 2 yaws left
        glm::vec3 yAxis = (float)sin(-rads/2) * this->m_spaceshipPlayer2->get_y_axis();
        this->m_spaceshipPlayer2->rotate(glm::quat(cos(-rads/2), yAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_KP_6) == GLFW_PRESS) { // Spaceship 2 yaws right
        glm::vec3 yAxis = (float)sin( rads/2) * this->m_spaceshipPlayer2->get_y_axis();
        this->m_spaceshipPlayer2->rotate(glm::quat(cos( rads/2), yAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_KP_7) == GLFW_PRESS) { // Spaceship 2 yaws left
        glm::vec3 zAxis = (float)sin(-rads/2) * this->m_spaceshipPlayer2->get_z_axis();
        this->m_spaceshipPlayer2->rotate(glm::quat(cos(-rads/2), zAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_KP_9) == GLFW_PRESS) { // Spaceship 2 yaws right
        glm::vec3 zAxis = (float)sin( rads/2) * this->m_spaceshipPlayer2->get_z_axis();
        this->m_spaceshipPlayer2->rotate(glm::quat(cos( rads/2),zAxis));
    }
    if (glfwGetKey(window, GLFW_KEY_KP_0) == GLFW_PRESS) { // Spaceship 2 shoot
        std::shared_ptr<Missile> missile = m_spaceshipPlayer2->generate_missile();
        if(missile != nullptr) {
            m_entities[ModelKeys::LASER_2].push_back(missile);
        }
    }
}

void GlApplication::resize(GLFWwindow * window, int framebufferWidth, int framebufferHeight)
{
    GlApplication & app = *static_cast<GlApplication *>(glfwGetWindowUserPointer(window));

    float aspect = framebufferWidth / float(framebufferHeight);
    app.m_proj = glm::perspective(80.f, aspect, 0.1f, (float)(SKYBOX_SIZE + 100));

    glViewport(0, 0, framebufferWidth, framebufferHeight);
    
    app.setFramebufferSize(framebufferWidth, framebufferHeight);
}

void GlApplication::keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
    GlApplication & app = *static_cast<GlApplication *>(glfwGetWindowUserPointer(window));
    if(action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_L:
                app.quickLog();
                break;

            case GLFW_KEY_N:
                app.toggleHitboxes();
                break;
        }
    }
}

void GlApplication::setFramebufferSize(int framebufferWidth, int framebufferHeight) {
    m_framebufferWidth = framebufferWidth;
    m_framebufferHeight = framebufferHeight;
}
