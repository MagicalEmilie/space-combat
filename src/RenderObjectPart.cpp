#include "RenderObjectPart.hpp"

RenderObjectPart::RenderObjectPart(std::shared_ptr<VAO> vao, std::shared_ptr<Program> program, std::shared_ptr<Texture> texture, std::shared_ptr<Texture> ntexture,
        std::shared_ptr<Texture> stexture)
    : m_vao(vao), m_program(program), m_diffuseTexture(texture), m_normalTexture(ntexture), m_specularTexture(stexture)
{
}

void RenderObjectPart::draw(Sampler * colormap, Sampler * normalmap, Sampler * specularmap)
{
    m_program->bind();
    colormap->attachTexture(*m_diffuseTexture);
    normalmap->attachTexture(*m_normalTexture);
    specularmap->attachTexture(*m_specularTexture);
    m_vao->draw();
    m_program->unbind();
}

void RenderObjectPart::update(const glm::mat4 & proj, const glm::mat4 & view, const glm::mat4 & mw)
{
    m_program->bind();
    m_program->setUniform("M", mw);
    m_program->setUniform("V", view);
    m_program->setUniform("P", proj);
    m_program->setUniform("positionCameraInWorld", glm::vec3(glm::inverse(view) * glm::vec4(0, 0, 0, 1)));

    m_program->unbind();
}
