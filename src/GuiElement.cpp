#include "GuiElement.hpp"

GuiElement::GuiElement(Program &program) 
		: vao(2),
		  textureSampler(0),
		  texture(new Texture(GL_TEXTURE_2D)),
		  position(0, 0),
		  dimension(1, 1) {
	createElement(program);
}

GuiElement::GuiElement(Program &program, const std::string &texturePath) 
		: vao(2),
		  textureSampler(0),
		  texture(new Texture(GL_TEXTURE_2D)),
		  position(0, 0),
		  dimension(1, 1) {
	createElement(program);

	Image<> rgbMapImage;
	std::string rgbFilename = absolutename(texturePath);
	stbi_set_flip_vertically_on_load(true);
	rgbMapImage.data = stbi_load(rgbFilename.c_str(), &rgbMapImage.width, &rgbMapImage.height, &rgbMapImage.channels, STBI_default);
	stbi_set_flip_vertically_on_load(false);
	texture->setData(rgbMapImage, true);

	this->textureSampler.attachTexture(*this->texture.get());
}

void GuiElement::createElement(Program &program) {
	std::vector<glm::vec2> vbo = {glm::vec2(0, 0), glm::vec2(0, 1), glm::vec2(1, 1), glm::vec2(1, 0)};
	std::vector<glm::vec2> uv = {glm::vec2(0, 0), glm::vec2(0, 1), glm::vec2(1, 1), glm::vec2(1, 0)};
	std::vector<uint> ibo = {0, 2, 1, 2, 0, 3};

	vao.setVBO(0, vbo);
	vao.setVBO(1, uv);
	vao.setIBO(ibo);
	
	this->textureSampler.setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    this->textureSampler.setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    this->textureSampler.setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    this->textureSampler.setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
    this->textureSampler.attachToProgram(program, "textureMap", Sampler::BindUnbind);
	
	computeModelMatrix();
}

void GuiElement::draw() {
	this->textureSampler.bind();
	this->textureSampler.attachTexture(*this->texture.get());

	vao.draw();

	this->textureSampler.unbind();
}

void GuiElement::updateUniforms(Program &guiProgram) {
	guiProgram.setUniform("M", this->modelMatrix);
	
    glm::mat4 p = glm::ortho(0, 1, 0, 1, 1, -1);
    guiProgram.setUniform("P", p);
}

void GuiElement::computeModelMatrix() {
	glm::mat4 translateMatrix = glm::translate(glm::mat4(1), glm::vec3(this->position, 0));
    glm::mat4 scaleMatrix     = glm::scale(glm::mat4(1), glm::vec3(this->dimension, 1));

    this->modelMatrix = translateMatrix * scaleMatrix;
}

void GuiElement::setPosition(glm::vec2 position) {
	this->position = position;

	computeModelMatrix();
}

void GuiElement::setDimension(glm::vec2 dimension) {
	this->dimension = dimension;

	computeModelMatrix();
}


void GuiElement::setTexture(std::shared_ptr<Texture> &newTexture) {
	this->texture = newTexture;
}
