#version 410

in vec2 uv;

uniform sampler2D textureMap;

out vec4 fragColor;

void main()
{
    fragColor = vec4(texture(textureMap, uv).rgb, 1);
}
